'''
Created on 1 dic. 2019

@author: eduardo
'''

import lucidity
import pprint
import os
import glob


# define a template
asset_def = '{base_dir:[\w_.\-:]+}/prj/{project}/assets/{asset_type}/{asset_name}/{step_name}/{asset_type}_{asset_name}_{step_name}_{task_name}_{name}_v{version}.{filetype}'
asset_template = lucidity.Template('asset_component', asset_def,
                                   duplicate_placeholder_mode=lucidity.Template.STRICT)
# duplicate_placeholder_mode=lucidity.Template.STRICT -> all equal keys must have equal value

# lets test it with a path
asset_path = 'W:/prj/ABC/assets/prop/table/model/prop_table_model_uvs_master_v001.ma'

# parse the path to get data out of it
data = asset_template.parse(asset_path)
print 'Data from path:'
pprint.pprint(data)

# change a key and build a new path
data['asset_name'] = 'chair'
path = asset_template.format(data)
print 'Builded path with chair instead of table'
pprint.pprint(path)

# define another template
shot_def = '{base_dir:[\w_.\-:]+}/prj/{project}/seq/{seq_name}/{shot_name}/{step_name}/{shot_name}_{step_name}_{task_name}_{name}_v{version}.{filetype}'
shot_template = lucidity.Template('shot_component', shot_def,
                                   duplicate_placeholder_mode=lucidity.Template.STRICT)

# but his time, lets find between many templates
prj_templates = [asset_template, shot_template]
data, template = lucidity.parse(asset_path, prj_templates)
print 'Founded this data'
pprint.pprint(data)
print 'with this template', template.name
pprint.pprint(template)

# lets thest with another path
shot_path = 'W:/prj/ABC/seq/WOW/WOW-1050/anim/WOW-1050_anim_keys_master_v002.mb'

data, template = lucidity.parse(shot_path, prj_templates)
print 'Founded this data'
pprint.pprint(data)
print 'with this template', template.name
pprint.pprint(template)

# define templates for a project

# 1) load from a folder with predefined py files
folder_where_this_file_is = os.path.dirname(__file__)
template_folder = os.path.join(folder_where_this_file_is, 'templates')

paths_list = [template_folder]
templates = lucidity.discover_templates(paths=paths_list, recursive=True)

# test that it worked out fine
data, template = lucidity.parse('/jobs/aec857s99as8d76s', templates)
print 'Founded this data'
pprint.pprint(data)
print 'with this template', template.name
pprint.pprint(template)

# 1) now lets use the specia environmental var
os.environ['LUCIDITY_TEMPLATE_PATH'] = template_folder
templates_2 = lucidity.discover_templates()

print 'Result is the same?', len(templates) == len(templates_2)

# lets test it
data, template = lucidity.parse('/jobs/1234/shots/test_5678', templates_2)
print 'Founded this data'
pprint.pprint(data)
print 'with this template', template.name
pprint.pprint(template)

# reference other templates in a template

# 'W:/prj/ABC/seq/WOW/WOW-1050/anim/WOW-1050_anim_keys_master_v002.mb'
shot_base_template = lucidity.Template('shot_base', '{base_dir:[\w_.\-:]+}/prj/{project}/seq/{seq_name}/{shot_name}',
                                   duplicate_placeholder_mode=lucidity.Template.STRICT)
shot_template_2 = lucidity.Template('shot_component2', '{@shot_base}/{step_name}/{shot_name}_{step_name}_{task_name}_{name}_v{version}.{filetype}',
                                   duplicate_placeholder_mode=lucidity.Template.STRICT)
shot_template_2.template_resolver = {'shot_base': shot_base_template}

# parse a path
data = shot_template_2.parse(shot_path)
print 'Founded this data'
pprint.pprint(data)

# create a folder structure with templates by generating paths and creating folders and empty files

data = asset_template.parse(asset_path)

# define base folder
data['base_dir'] = 'D:'  # <-- change path

asset_list = ['table', 'chair', 'tvset', 'lamp']

# define some tasks related to each step
task_names = {'model': ['hero', 'low', 'cloth'], 'rig': ['animation', 'deform', 'layout'],
              'light': ['indoor', 'outdoor']}

for asset_name in asset_list:

    for step_name in task_names.keys():

        # define fields
        data['asset_name'] = asset_name
        data['step_name'] = step_name
        for task_name in task_names[step_name]:
            data['task_name'] = task_name

            # build path
            builded_path = asset_template.format(data)

            # create folder
            folder = os.path.dirname(builded_path)
            if not os.path.isdir(folder):
                os.makedirs(folder)

            # create file
            with open(builded_path, 'w'):
                pass
            print('created {}'.format(builded_path))

# open in windows explorer to see the resutls
cmd = 'explorer.exe "{}"'.format(data['base_dir'])
os.system(cmd)

# listing files with the help of glob

# to build a glob replace the keys with *
data['asset_name'] = '*'
data['version'] = '*'
glob_path = asset_template.format(data)
print 'glob path\n', glob_path

# now list the files
found_files = glob.glob(glob_path)
print 'files found for light key of any asset'
pprint.pprint(found_files)

# look for all steps for chair
data['asset_name'] = 'chair'
data['step_name'] = '*'
data['task_name'] = '*'
data['name'] = '*'
data['version'] = '*'
data['filetype'] = '*'
glob_path2 = asset_template.format(data)
print 'glob path\n', glob_path2

found_files = glob.glob(glob_path2)
print 'files found for chair in any step'
pprint.pprint(found_files)


# strict file list (this probably should be a function by itself)
found_files = glob.glob(glob_path2)
# add strage file to check
found_files.append('C:\\test\\wrong\\file.xyz')
found_files.append('D:/prj/ABC/assets/prop/house\light\prop_house_light_key_master_v001.ma')
strict_checked_files = []
for f in found_files:
    try:
        norm_path = os.path.normpath(f).replace('\\', '/')
        result = asset_template.parse(norm_path)
        if result['asset_name'] != 'chair':
            continue
    except Exception, e:
        continue
    strict_checked_files.append(f)

print 'files found for chair in any step STRICTLY CHECKED'
pprint.pprint(strict_checked_files)

