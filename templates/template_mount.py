'''
Created on 1 dic. 2019

@author: eduardo
'''
# templates.py

from lucidity import Template

def register():
    '''Register templates.'''
    return [
        Template('shot_component',
    '{base_dir}/prj/{project}/seq/{seq_name}/{shot_name}/{step}/{shot_name}_{task}_{step}_{name}_v{version}.{filetype}',
                                   duplicate_placeholder_mode=Template.STRICT),
        Template('asset_component',
    '{base_dir}/prj/{project}/assets/{asset_type}/{asset_name}/{step_name}/{asset_type}_{asset_name}_{task}_{name}_v{version}.{filetype}',
                                   duplicate_placeholder_mode=Template.STRICT),
    ]